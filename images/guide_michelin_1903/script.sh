#!/bin/sh
# SPDX-FileCopyrightText: 2023 Haelwenn (lanodan) Monnier <contact+blog@hacktivis.me>
# SPDX-License-Identifier: MIT

set -e

mkdir -p thumbs

for img in *.JPG; do
	test -e "./thumbs/$img" && continue
	gm convert "$img" -resize 25% -auto-orient -quality 69% "./thumbs/$img"
	jpegoptim -s "./thumbs/$img"
done
