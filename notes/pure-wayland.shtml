<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
<!--#include file="/templates/head.shtml" -->
		<title>non-XOrg Wayland — lanodan’s cyber-home</title>
	</head>
	<body>
<!--#include file="/templates/en/nav.shtml" -->
		<main>
			<h1>non-XOrg Wayland</h1>
			<p><strong>2022-05 note</strong>: It's getting really rare for something to not work unless it's stuck to X11 or it's badly linking to OpenGL, consider that most things "Just Works", unless I described a problem and it's still there.</p>
			<p>
				<strong>Warning:</strong> This is Wayland without any of <code>libX11</code> or related (ie. <code>libGL.so</code> instead of <code>libOpenGL.so</code>) in the system, this also removes the <code>XWayland</code> compatibility layer as it's an xorg-server component.<br/>
				You can find this kind of similar setup in embedded systems (ie. SailfishOS), as X11 makes no sense outside of keyboard+mouse setups, but I don't expect it until a bunch of years for regular desktops (say when X.Org will have died), I'm doing this right now to avoid having to rush to get rid of X11 with having to push the End-of-Life date way back it's upstream date (that happened with python-2 because it wasn't much prepared until the last year or two).<br/>
				It's also the kind of setup that I expect soon in security-oriented systems because Wayland addresses a number of design flaws.
			</p>
			<p>Testing is done on Gentoo Linux with <code>USE="-X -XWayland"</code> and <del><code><kbd>-DEGL_NO_X11</kbd></code> into the <code>C(XX)FLAGS</code></del>(fixed via <a href="https://gitlab.freedesktop.org/glvnd/libglvnd/-/merge_requests/246">patching libglvnd</a>). And launched into the <a href="https://swaywm.org">Sway compositor</a>.</p>
			<p>Reminder:
				<dl>
					<dt>static linking</dt><dd>(<code>LDFLAGS="-static"</code>) some of the library is copied in the executable</dd>
					<dt>shared linking</dt><dd>(<code>LDFLAGS="-shared"</code>) system library like <code>ld-linux-x86-64.so.2</code> loading the libraries before execution</dd>
					<dt>dynamic <del>linking</del> loading</dt><dd>(<code>dlopen()</code>) loading a library during the execution</dd>
				</dl>
			</p>

			<h2 id="novidya">NoVidya reminder <a class="hlink" href="#novidya">§</a></h2>
			<p>Do not ask Libre software developers for support of the NVidia proprietary driver, specially when they already refused, the driver being proprietary means that any bug found has to be worked around, which often isn't possible without compromising performance and stability.</p>
			<p>Also, in terms of history:</p>
			<ul>
				<li>After almost 10 years NVidia has yet to provide a complete enough driver for Wayland</li>
				<li><del>Instead of providing the GBM API like everyone else, including other proprietary drivers (there is a lot of them on the embedded side of things), they created EGLStreams, which means a very large chunk of code (and so bugs) specific to NVidia</del></li>
				<li>Contributes to a huge waste of chips by stopping the support of GPUs after about 5 years</li>
				<li><del>Has basically been trashing any hope for nouveau to be a complete driver (check FOSDEM videos of the nouveau developers)</del></li>
				<li>NVidia performance vs. AMDGPU performance on linux doesn't have a very large gap on real-life loads</li>
				<li><a href="https://forums.developer.nvidia.com/t/support-for-async-reprojection/123382">Doesn't provides async reprojection on linux, so no VR for you</a> (Or a lot of those nice Vulkan speedups)</li>
				<li><a href="https://en.wikipedia.org/wiki/Nvidia_Optimus#Linux_support">Remember how awful Nvidia Optimus is?</a></li>
				<li>Video decoder driver can have frequent glitches depending on the encoding</li>
			</ul>

			<h2 id="buildsystems">Buildsystems <a class="hlink" href="#buildsystems">§</a></h2>
			<p>I've yet to see one work correctly out of the box for this. The snippets in this section Abandons all copyrights if it could even begin to be copyrightable, as I can't assign to the Public Domain in France, they are distributed under any OSI/FSF-approuved license including CC-0, WTFPL, Unlicense and BSD-0.</p>
			<h3>CMake</h3>
			<ol>
				<li>Replace <code>find_package(OpenGL REQUIRED)</code> to something like <code>find_package(OpenGL COMPONENTS OpenGL REQUIRED)</code>. This allows to not error out when libGL.so isn't found but libOpenGL.so is.</li>
				<li>Replace <code>OPENGL_gl_LIBRARY</code> to <code>OPENGL_opengl_LIBRARY</code>. This will allow to use libOpenGL.so over libGL.so when possible.</li>
			</ol>
			<h3>Auto*hell</h3>
			<p>Either use <code>AX_CHECK_GL</code> from autoconf-archive with <a href="http://savannah.gnu.org/patch/index.php?10369">patch #10369: ax_check_gl.m4: Fallback to libOpenGL</a>, or use something like the following:</p>
<pre><code>AC_CHECK_HEADER(
	[GL/gl.h],
	AC_SEARCH_LIBS(glVertex3f, [opengl32 GL GL2 OpenGL], [test $ac_cv_search_glVertex3f = "none required" || GL_LIBS="$ac_cv_search_glVertex3f $GL_LIBS"], [AC_MSG_ERROR(*** OpenGL library not found on system!)]),
	[AC_MSG_ERROR(*** OpenGL headers not found on system!)]
)</code></pre>
			<h3>meson</h3>
			<p>Nice fail of an otherwise nice mainly declarative buildsystem there… (<a href="https://github.com/mesonbuild/meson/issues/3679">meson#3679</a>)</p>
<pre><code>gl_dep = dependency('gl', required: false)
if not gl_dep.found()
  # libglvnd fallback for libX11-less wayland systems
  gl_dep = dependency('opengl')
endif</code></pre>

			<h2 id="glapi">GL API coverage <a class="hlink" href="#glapi">§</a></h2>
			<p>All of this is tested on a "AMD Ryzen 5 PRO 3500U w/ Radeon Vega Mobile Gfx" aka "AMD Radeon(TM) Vega 8 Graphics (RAVEN)" aka:
<pre>
06:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Picasso (rev d2)
06:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Raven/Raven2/Fenghuang HDMI/DP Audio Controller
06:00.2 Encryption controller: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) Platform Security Processor
06:00.3 USB controller: Advanced Micro Devices, Inc. [AMD] Raven USB 3.1
06:00.4 USB controller: Advanced Micro Devices, Inc. [AMD] Raven USB 3.1
06:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/FireFlight/Renoir Audio Processor
06:00.6 Audio device: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) HD Audio Controller
</pre>
</p>
			<ul>
				<li><a href="https://github.com/cginternals/glbinding">glbinding</a> <code>glinfo</code> output: <a href="pure_wayland_T495_glinfo.log">pure_wayland_T495_glinfo.log</a></li>
				<li><a href="http://glew.sourceforge.net/">GLEW</a> <code>glewinfo</code> output: <a href="pure_wayland_T495_glewinfo.log">pure_wayland_T495_glewinfo.log</a></li>
			</ul>

			<h2 id="libs">Librairies <a class="hlink" href="#libs">§</a></h2>
			<h3>Entirely works out of the box</h3>
			<ul>
				<li><a href="https://www.glfw.org/">GLFW</a>: Yes, Just Works</li>
				<li><a href="https://github.com/cginternals/glbinding">glbinding</a>: Yes, Just Works</li>
				<li><a href="https://www.libsdl.org/">SDL2</a> (2.0.16): Yes, works out of the box. Some workarounds for previous versions being:<ul>
					<li>For 2.0.14 and earlier: Needing to set <code>SDL_VIDEO_GL_DRIVER=libOpenGL.so</code> or patch libSDL2 (<a href="https://github.com/libsdl-org/SDL/issues/4158">SDL#4158</a>).</li>
					<li>For 2.0.14 and earlier, you'll need <a href="https://github.com/libsdl-org/SDL/pull/4358">libsdl-org/SDL#4358</a> for desktop OpenGL</li>
					<li>When bundled in binairies: Needing to set <code><a href="https://github.com/libsdl-org/SDL/blob/main/docs/README-dynapi.md">SDL_DYNAMIC_API</a></code> to where your distro's <code>libSDL2.so</code> library is.</li>
				</ul></li>
				<li><a href="https://github.com/libsdl-org/sdl12-compat">SDL-1.2 (compat)</a>: Might still have few papercuts but overall it just works</li>
				<li><a href="https://love2d.org/">löve(2d)</a> (0.10.2+, tested with 11.3 + lua5.2): Yes, it's SDL2-based after all.</li>
				<li><a href="http://www.waffle-gl.org/">waffle</a> Yes, use 1.7.0+ for support in modern compositors</li>
				<li><a href="http://mygui.info">MyGUI</a> (3.4.2+): Yes, earlier versions needed a patch to not pull libGL and GLX (<a href="https://github.com/MyGUI/mygui/pull/224">mygui#224</a>)</li>
			</ul>
			<h3>Needs more testing</h3>
			<ul>
				<li><a href="https://liballeg.org/">allegro-4</a>: Maybe, compiles with OpenGL enabled</li>
			</ul>
			<h3>Needs workarounds / Incomplete support</h3>
			<ul>
				<li><a href="https://github.com/anholt/libepoxy">libepoxy</a>: Needs <a href="https://github.com/anholt/libepoxy/pull/259">libepoxy#259</a> for <code>libOpenGL.so</code> and then it just works</li>
				<li><a href="https://godotengine.org">godot</a>: Wayland support since <a href="https://github.com/godotengine/godot/pull/86180">Add Wayland support - PR86180</a></li>
				<li><a href="https://wxwidgets.org/">wxWidgets GTK port, 3.1.x</a> (3.1.5): Needs a patch to remove an harcoded <code>-lX11</code></li>
				<li><a href="https://www.gtk.org/">GTK</a> 3: Almost. <a href="https://gitlab.gnome.org/GNOME/gtk/-/issues/699">Suggestions are broken on multi-monitors</a> as well as the usual glitches.</li>
				<li><a href="https://www.qt.io/">Qt</a> 5: Almost, menus are misplaced, specially on multi-monitors and requires <code>-lGL</code> → <code>-lOpenGL</code> workaround</li>
				<li><a href="http://glew.sourceforge.net/">GLEW: OpenGL Extension Wrangler Library</a>: <a href="https://github.com/nigels-com/glew/issues/172">Not really</a>, consider <a href="https://github.com/anholt/libepoxy#why-not-use-libglew">libepoxy</a> or <a href="https://glbinding.org/">glbinding</a> (drop-in). I managed to get it to compile and work for some software with:<ul>
					<li>Applying <a href="https://github.com/nigels-com/glew/pull/216/commits/584a266e71c3c3f7ec4a0e4e61c31b67f80a2419">Extend GLEW API to support glewContextInit - GL without implied GLX, … #216</a></li>
					<li>Running <code><kbd>sed -i 's/-lGL/-lOpenGL/' config/Makefile.linux-egl</kbd></code></li>
					<li>Passing <code>POPT="${CFLAGS} <kbd>-D__native_client__</kbd>"</code> and <code><kbd>SYSTEM=linux-egl</kbd></code> to make options, for both build and install</li>
				</ul></li>
				<li><a href="https://www.gtk.org/">GTK</a> 4: No, unless maybe you use GNOME, which means it's not even FreeDesktop portable</li>
				<li><a href="http://freeglut.sourceforge.net/">FreeGLUT: free OpenGL utility toolkit</a>: No, depends on <code>wl_shell</code> which is absent in wlroots and probably others (See <a href="https://github.com/dcnieho/FreeGLUT/issues/72">FreeGLUT#72</a>), it's activity is very low so consider using other librairies instead</li>
				<li><a href="https://liballeg.org/">allegro-5</a>: I managed to get most of it to compile with some patches, couldn't get liballegro_dialog (gtk on non-apple unixes) yet though as it depends on GTK with X11.</li>
			</ul>
			<h3>Doesn't Works</h3>
			<ul>
				<li><a href="http://irrlicht.sourceforge.net/">irrlicht</a>: Nope, <a href="http://irrlicht.sourceforge.net/forum/viewtopic.php?f=6&amp;t=52107">there is some community work on it</a></li>
				<li><a href="https://www.tcl.tk/">TCL/TK</a>: Nope, but there is <a href="http://www.androwish.org/home/wiki?name=undroidwish">undroidwish</a> apparently supporting it, <a href="https://wiki.tcl-lang.org/page/GSoC+Idea%3A+Tk+Backend+for+the+Wayland+Display+Protocol">GSoC idea page</a></li>
				<li><a href="https://www.fltk.org/">FLTK</a>: Nope, could be there in the future(1.4.x) see <a href="https://www.fltk.org/str.php?L3371">STR #3371</a></li>
				<li><a href="https://www.sfml-dev.org/">SFML: Simple and Fast Multimedia Library</a>: <a href="https://github.com/SFML/SFML/issues/1082">Nope</a></li>
				<li><a href="https://ibiblio.org/ggicore/packages/libggi.html">General Graphics Interface(GGI)</a>: Doesn't seems to have wayland support</li>
				<li><a href="https://wxwidgets.org/">wxWidgets GTK port, 3.0.x</a>: No, GUI depends on X11, also needs <code>-lGL</code> → <code>-lOpenGL</code> patch</li>
				<li><a href="http://plib.sourceforge.net/">plib</a>: Dead library (last commit in 2012, last release in 2006), has security issues</li>
				<li><a href="https://gitlab.gnome.org/GNOME/cogl">cogl</a>: Dead library (Archived), hardcodes for <code>-lGL</code></li>
				<li><a href="https://gitlab.gnome.org/GNOME/clutter">clutter</a>: Dying library (replaced by GTK 4.0), depends on cogl, <a href="https://gitlab.gnome.org/GNOME/clutter/-/merge_requests/5">doesn't supports xdg-shell</a></li>
			</ul>

			<h2 id="browsers">Web Browsers <a class="hlink" href="#browsers">§</a></h2>
			<ul>
				<li><a href="/projects/badwolf">Badwolf</a>: Yes, Fully tested and supported as first-class citizen</li>
				<li><a href="https://wiki.gnome.org/Apps/Web">epiphany - Gnome Web</a>: Yes, seems to work fine, should be well supported</li>
				<li><a href="https://www.netsurf-browser.org/">netsurf</a>: GTK3 frontend just works</li>
				<li><a href="https://chromium.org/">chromium</a>: Not really, requires to enable ozone, which means compiling it, something I'd rather not do</li>
				<li><a href="https://www.mozilla.com/firefox">Mozilla Firefox</a>: No, segfaults when compiled without X11 libs. Wayland support is a horrible mess (ie. microphone/camera grab is a top-level window); screen-grabbing uses GNOME's xdg-desktop-portal, which is dbus-based instead of wayland-based <code>wlr_screencopy</code>.</li>
			</ul>

			<h2 id="games">Games <a class="hlink" href="#buildsystems">§</a></h2>
			<h3>Entirely works out of the box</h3>
			<ul>
				<li><a href="https://github.com/fmang/oshu">oshu</a>: Yes, just Works (SDL2)</li>
				<li><a href="https://parallelrealities.itch.io/blob-wars-attrition">Blob Wars : Attrition</a>: Yes, Just Works (SDL2)</li>
				<li><a href="https://github.com/TerryCavanagh/vvvvvv">VVVVVV</a>: Yes, Just Works (SDL2)</li>
				<li><a href="https://ptilouk.net/superflu-riteurnz/">Superflu Riteurnz</a>: Yes, just Works (SDL2)</li>
				<li><a href="https://github.com/Xenoveritas/abuse">abuse</a> (Crack.Com): Yes, Just Works</li>
				<li><a href="https://github.com/sergiou87/open-supaplex">open-supaplex</a>: Just Works</li>
				<li><a href="https://github.com/nxengine/nxengine-evo">nxengine-evo</a> (Cave Story modern engine): Just Works</li>
				<li>StabYourself games compatible with love-11.3 (
					<a href="https://github.com/Stabyourself/not-tetris-3">not-tetris-3</a>,
					<a href="https://github.com/Stabyourself/text-based-golf">text-based-golf</a>,
					<a href="https://github.com/Stabyourself/text-based-sokoban">text-based-sokoban</a>,
					<a href="https://github.com/Stabyourself/mari0">mari0</a>,
					<a href="https://github.com/Stabyourself/notdonkeykong">notdonkeykong</a>,
					<a href="https://github.com/Stabyourself/orderoftwilight">orderoftwilight</a>,
					…): Just Works</li>
				<li><a href="https://7kfans.com/">seven-kingdoms</a>: Seems to work fine</li>
			</ul>

			<h3>Light temporary workaround caused by dependency</h3>
			<ul>
				<li><a href="https://taisei-project.org/">taisei</a>: Need to set <code>SDL_VIDEO_GL_DRIVER=libOpenGL.so</code> or patch libSDL2 (<a href="https://github.com/libsdl-org/SDL/issues/4158">SDL#4158</a>).</li>
				<li><a href="https://github.com/adoptware/pinball">Emilia Pinball</a>: Either SDL2 with GLES1 support (<a href="https://github.com/gentoo/gentoo/pull/20690">gentoo/gentoo#20690</a>) and passing <code>--enable-gles</code> or applying <a href="https://github.com/libsdl-org/SDL/pull/4358">libsdl-org/SDL#4358</a> to get desktop OpenGL working</li>
				<li><a href="http://chromium-bsu.sourceforge.net/">Chromium B.S.U</a> (0.9.16.1+): With SDL2 fixed for desktop OpenGL it works</li>
				<li><a href="https://viewizard.com">astromenace</a> (1.4.1): With SDL2 fixed for desktop OpenGL it works</li>
				<li><a href="https://www.openra.net/">OpenRA</a>: With SDL2 fixed for desktop OpenGL it works</li>
				<li><a href="https://xmoto.tuxfamily.org">xmoto</a>: With SDL2 fixed for desktop OpenGL it works</li>
			</ul>

			<h3 id="games-sdl12compat">Just needs SDL-1.2 compatibility library <a href="#games-sdl12compat">§</a></h3>
			<ul>
				<li><a href="http://www.mhgames.org/oldies/formido/">formido</a>: With sdl12-compat it just works</li>
				<li><a href="http://heroes.sourceforge.net/">heroes</a>: Works with sdl12-compat, I just get a very small default window but fullscreen works</li>
				<li><a href="http://freshmeat.sourceforge.net/projects/luola">luola</a>: With sdl12-compat it just works</li>
				<li><a href="http://icculus.org/oes/">orbital-eunuchs-sniper / sniper2d</a>: With sdl12-compat it just works</li>
				<li><a href="https://linux.tlk.fr/">powermanga</a>: With sdl12-compat it just works</li>
				<li><a href="https://cyp.github.com/snipes/">snipes</a>: With sdl12-compat it seems to work???</li>
				<li><a href="http://www.towbowltactics.com/index_en.html">towbowl-tactics</a>: With sdl12-compat it seems to work???</li>
				<li><a href="http://abe.sourceforge.net/">abe</a>: With sdl12-compat it seems to just work</li>
				<li><a href="http://www.fishies.org.uk/apricots.html">apricots</a>: With sdl12-compat it seems to just work</li>
				<li><a href="http://makegho.mbnet.fi/c/bchase/">balloonchase</a>: With sdl12-compat it seems to just work</li>
				<li><a href="http://perso.b2b2c.ca/~sarrazip/dev/batrachians.html">batrachians</a>: With sdl12-compat it seems to just work</li>
				<li><a href="https://savannah.nongnu.org/projects/clanbomber/">clanbomber</a>: With sdl12-compat <a href="https://github.com/libsdl-org/sdl12-compat/commit/1cc3061c06877a7ee4277187aa59dcca755e5a7f">1cc3061c06877a7ee4277187aa59dcca755e5a7f</a> and later it seems to just work</li>
				<li><a href="https://github.com/petarov/savagewheels">savagewheels</a>: With sdl12-compat it seems to just work</li>
				<li><a href="http://koti.mbnet.fi/makegho/c/betna/">betna</a>: With sdl12-compat it seems to just work</li>
				<li><a href="http://www.linux-games.com/bumprace/">bumprace</a>: With sdl12-compat it seems to just work</li>
				<li><a href="http://www.losersjuegos.com.ar/juegos/ceferino">ceferino</a>: With sdl12-compat it seems to just work</li>
				<li><a href="http://www.newbreedsoftware.com/circus-linux/">circuslinux</a>: With sdl12-compat it seems to just work</li>
				<li><a href="http://zapek.com/?p=189">syobon</a>: With sdl12-compat it seems to just work</li>
			</ul>

			<h3>Needs workarounds / Incomplete support</h3>
			<ul>
				<li><a href="https://supertux.org/">SuperTux</a>: <del>No, because of GLEW</del> Yes, changed it's build configuration to use <a href="https://glbinding.org/">glbinding</a></li>
				<li><a href="http://endless-sky.github.io/">endless-sky</a>: Almost, had to change linking from <code>GL</code> to <code>OpenGL</code> in <code>SConstruct</code>, no proper detection done</li>

				<li><a href="http://www.freeciv.org/">freeciv</a>: Yes(SDL2 and Qt5 clients) and No(GTK+2 client, the default/goto one)</li>
				<li><a href="https://supertuxkart.net/">SuperTuxKart</a>: Quite, works in wayland with gles2(because irrlicht tries to dynamically load <code>libGL.so.1</code> otherwise) in current git (<code>9f1ae2b7619f21546cc5cdd4bbeea53492d4b7ac</code>)</li>
				<li><a href="https://github.com/FWGS/xash3d-fwgs">Xash3D FWGS</a> + <a href="https://github.com/FWGS/hlsdk-xash3d">HLSDK Xash3D</a> (Half-Life1 engine): Yes, has some glitches but seems unrelated to Wayland. Note: You need to set <code>SDL_VIDEO_GL_DRIVER=libOpenGL.so</code> or <code>BOX86_LIBGL=libOpenGL.so</code></li>
				<li><a href="https://neverball.org">neverball</a> (1.6.0): Works with OpenGL ES(<code>make ENABLE_OPENGLES=1</code>) and desktop OpenGL with <code>make OGL_LIBS=-lOpenGL</code></li>
				<li><a href="https://gitlab.com/pingus/pingus">pingus</a>: Displays with a small patch(<a href="https://gitlab.com/pingus/pingus/-/merge_requests/191">pingus!191</a>), story mode segfaults but seems unrelated.</li>
				<li><a href="https://nc.itch.io/mercury">Excavation Site Mercury</a>: Need to set <code><a href="https://github.com/libsdl-org/SDL/blob/main/docs/README-dynapi.md">SDL_DYNAMIC_API</a></code> to where your distro's <code>libSDL2.so</code> library is</li>
				<li><a href="https://www.hempuli.com/baba/">Baba Is You</a> (Humble Bundle): Needs to run <code><a href="https://nixos.org/patchelf.html">patchelf</a> --replace-needed libGL.so.1 libOpenGL.so.0 bin64/Chowdren</code> and set <code><a href="https://github.com/libsdl-org/SDL/blob/main/docs/README-dynapi.md">SDL_DYNAMIC_API</a></code> to where your distro's <code>libSDL2.so</code> library is</li>
				<li><a href="https://www.bzflag.org/">bzflag</a> (2.4.22): Yes, done <code>sed -i -e 's;X_PRE_LIBS -lX11;X_PRE_LIBS;' -e's;\[opengl32 GL GL2\];[opengl32 GL GL2 OpenGL];' configure.ac</code>; Requires SDL2 fixed for desktop OpenGL</li>
				<li><a href="http://armagetronad.org/">armagetronad</a> (trunk at <code>0ba8c6ff0aeda14d86760a1744c6296cbf8632be</code> + light fixes): With SDL2 fixed for desktop OpenGL it works</li>
				<li><a href="https://www.teeworlds.com/">teeworlds</a> (0.7.5 + master): Needed a small patch to use GLVND and remove an unused X11 check (<a href="https://github.com/teeworlds/teeworlds/pull/2875">teeworlds#2875</a>)</li>
				<li><a href="http://quakespasm.sourceforge.net/">QuakeSpasm</a> (master; Quake1 engine): With <code>USE_SDL2=1 COMMON_LIBS="-lm -lOpenGL"</code> passed to <code>make</code> it just works.</li>
				<li><a href="http://www.libsdl.org/projects/Maelstrom/">Maelstrom</a> (3.0.6-r3): <code>double free or corruption (out)</code></li>
				<li><a href="http://www.asahi-net.or.jp/~cs8k-cyu/windows/rr_e.html">rrootage</a>: sdl12-compat and replacing a <code>-lGL</code> to <code>-lOpenGL</code></li>
				<li><a href="https://violetland.github.io/">violetland</a>: With sdl12-compat and tweaking the CMakeList.txt for GLVND it just works</li>
			</ul>

			<h3>Almost</h3>
			<ul>
				<li><a href="http://blockrage.sourceforge.net/">blockrage</a>: With sdl12-compat it seems to just work but it aborts on exit</li>
				<li><a href="http://www.autismuk.freeserve.co.uk/">Cruising on Broadway</a>: With sdl12-compat it seems to just work but it aborts on exit</li>
				<li><a href="http://www.cloudsprinter.com/software/conveysdl/">conveysdl</a>: With sdl12-compat it seems to just work but it aborts on exit</li>
			</ul>

			<h3>Doesn't works</h3>
			<ul>
				<li><a href="http://www.wesnoth.org">wesnoth</a>: No, some basic actions are broken (questionable hooks to xdg-open(1)/open(1) put behind a if-X11)</li>
				<li><a href="https://www.xonotic.org/">xonotic</a>: No, depends on X11</li>
				<li><a href="https://www.bay12games.com/dwarves">dwarf fortress</a>: No, hard-depends on GTK2+ and libSDL-1</li>
				<li><a href="http://www.secretmaryo.org/">Secret Maryo Chronicles</a>: No, hard depends on X11</li>
				<li><a href="https://secretchronicles.org/">TSC: The Secret Chronicles of Dr. M.</a>: No, because of SFML</li>
				<li><a href="https://extremetuxracer.sourceforge.net">Extreme Tux Racer</a>: No, because of SFML</li>
				<li><a href="https://github.com/Anuken/Mindustry">Mindustry</a>: Nope, <code>libsdl-arc64.so</code> is shared linked against <code>libGL.so.1</code></li>
				<li><a href="http://oolite.org/">oolite</a>: Nope, <a href="https://github.com/OoliteProject/oolite/issues/222">still on SDL-1.2</a></li>
				<li><a href="https://github.com/FluffyStuff/OpenRiichi">OpenRiichi</a>: Vala looks like a mess to make it work with <code>libOpenGL.so</code> instead.</li>
				<li><a href="https://opensurge2d.org/">OpenSurge</a>: No because it depends on <code>liballegro_dialog</code></li>
				<li><a href="https://fte.triptohell.info/">FTE Quake World</a>(Quake Game Engine): Binary works in weston (with the legacy <code>"wl_shell"</code>) but doesn't in sway, it fails to hook to <code>"xdg_wm_base"</code> properly. Their wayland code is a gigantic pile of hacks.</li>
				<li><a href="http://lgames.sourceforge.net/Barrage/">barrage</a>: <code>creating 640x480 bitmap ... Unknown pixel format</code></li>
				<li><a href="http://www.autismuk.freeserve.co.uk/">deathchase3d</a>: Game seems to be broken, quitting triggers an abort</li>
				<li><a href="http://linux.softpedia.com/get/GAMES-ENTERTAINMENT/Arcade/Shooting-Star-19754.shtml">shootingstar</a>: Fails to detect SDL_image</li>
				<li><a href="https://chazomaticus.github.io/asteroid/">asteroid</a>: No, hard-depends on GTK2+ (and libSDL-1.2)</li>
				<li><a href="https://anakreon.cz/?q=node/1">berusky</a>: No, hard-depends on GTK2+ (and libSDL-1.2)</li>
				<li><a href="http://gltron.sourceforge.net/">gltron</a>: Antique auto*hell harcoded for <code>-lGL</code>, breaks on <code>autoreconf-{2.13,2.64,2.69}</code></li>
				<!-- =games-arcade/bomns-0.99.2-r1::gentoo fails with -Werror=format-security -->
				<li><a href="http://briquolo.free.fr/en/index.html">games-arcade/briquolo</a>: Black screen :(</li>
				<li><a href="http://www.cloudsprinter.com/software/conveysdl/">crack-attack</a>: Hardcodes <code>-lGL -lXi -lX11</code></li>
			</ul>

			<h2 id="multimedia">Multimedia <a class="hlink" href="#multimedia">§</a></h2>
			<ul>
				<li><a href="https://mpv.io/">mpv</a>: Yes, Just Works</li>
				<li><a href="http://mypaint.org/">MyPaint</a>: Yes, Just Works</li>
				<li><a href="https://github.com/aearil/dustr">dustr</a> (light interactive cropping tool): Yes, Just Works</li>
				<li><a href="https://pwmt.org/projects/zathura/">zathura</a>: Yes, Just Works (including the MuPDF plugin)</li>
				<li><a href="https://wiki.gnome.org/Apps/Evince">evince</a>: Yes, Just Works</li>
				<li><a href="https://inkscape.org/">Inkscape</a>: Yes, Just Works</li>
				<li><a href="https://www.darktable.org/">darktable</a>: Yes, Just Works</li>
				<li><a href="https://www.gimp.org/">The GIMP</a> (2.99.6): Yes, Just Works</li>
				<li><a href="https://gstreamer.freedesktop.org/">gstreamer</a>: Mostly, only lacks wlr-based screen grabbing and <a href="https://gitlab.freedesktop.org/gstreamer/gst-plugins-base/-/merge_requests/1143">requires patching meson for GLVND</a> (included in 1.19.2+)</li>
				<li><a href="https://github.com/eXeC64/imv">imv</a> (IMage Viewer): Sort-of, needs a patch for <code>-lGL</code> to <code>-lOpenGL</code> (<a href="https://github.com/eXeC64/imv/pull/285">my PR for it</a>)</li>
				<li><a href="https://www.blender.org">blender</a>: Needed to patch their cmake files a bit for GLVND</li>
				<li><a href="https://krita.org/">Krita</a>: Works, including tablet support. Had to switch Qt to full OpenGL instead of GLES and remove qtx11extra stuff in two <code>CMakeList.txt</code> files.</li>
				<li><a href="https://mupdf.com/">MuPDF</a>: With GLUT enabled it uses it's own version of freeglut, seems to require changing the ebuild recipe a lot.</li>
				<li><a href="https://www.mumble.info/">Mumble</a>: No, requires libXext (<a href="https://github.com/mumble-voip/mumble/pull/4607">own PR for making it optionnal</a>)</li>
				<li><a href="https://obsproject.com">OBS Studio</a>: No, requires X11 libs including in feaneron's <code>egl-wayland</code> branch</li>
				<li><a href="http://azsky2.html.xdomain.jp/soft/azpainter.html">azpainter</a>: No, depends on X11</li>
			</ul>

			<h2 id="emu">Emulator / Compat-layer <a class="hlink" href="#emu">§</a></h2>
			<ul>
				<li><a href="https://www.scummvm.org/">SCUMMVM</a>: Yes, seems to just work</li>
				<li><a href="https://hatari.tuxfamily.org/">hatari</a>: Yes, seems to just work (SDL2)</li>
				<li><a href="http://www.qemu.org">QEMU</a>: Yes, screen resize is a bit bugged (needs unfocus-refocus in sway)</li>
				<li><a href="https://www.winehq.org/">Wine</a>: Quite, works with <a href="https://gitlab.collabora.com/alf/wine/">alf's wayland branch</a>(<code>ce0da9c9f646536df40c2fa2869482edee0b1b1e</code>) + <a href="./0001-autotools-Lazily-change-libGL-to-libOpenGL.patch">own lazy patch for libOpenGL.so</a></li>
				<li><a href="https://dosbox-staging.github.io/">DOSBox Staging</a> (0.76.0): Yes, without OpenGL</li>
				<li><a href="http://melonds.kuribo64.net">MelonDS</a>: <a href="https://github.com/melonDS-emu/melonDS/pull/1722">trivial patch to disable X11 dependency</a></li>
				<li><a href="https://www.gnu.org/software/emacs/">GNU EMACS</a>: Soon, works in <code>feature/pgtk</code> branch and passing <code>--with-pgtk</code> to <code>./configure</code></li>
				<li><a href="https://9fans.github.io/plan9port/">plan9port</a>: No, doesn't seems to have a wayland port yet</li>
			</ul>

			<h2 id="misc">Misc <a class="hlink" href="#misc">§</a></h2>
			<ul>
				<li><a href="https://github.com/MasterQ32/kristall">kristall</a> (Qt Gemini Browser): Yes, Just Works</li>
				<li><a href="https://stellarium.org/">stellarium</a> (0.21.0): Yes, just works</li>
				<li><a href="http://www.gnumeric.org/">gnumeric</a>: Yes, seems to work fine</li>
				<li><a href="https://github.com/xournalpp/xournalpp">Xournal++</a> (1.0.20): Almost, needed a patch(<a href="https://github.com/xournalpp/xournalpp/pull/2934">xournalpp#2934</a>) to make X11 optional, included in 1.1.0+</li>
				<li><a href="https://gitlab.freedesktop.org/mesa/demos">Mesa Demos</a>: Nope</li>
				<li><a href="http://www.abisource.com/">abiword</a> (3.0.5): Nope, hard-dependency on X11</li>
				<li><a href="https://celestia.space">celestia</a> (<code>22291cd795606a73226f3c7b0acff65bdbbf3f24</code>): Nope, blank rendering</li>
			</ul>

			<h2>See Also</h2>
			<ul>
				<li><a href="https://github.com/natpen/awesome-wayland">Awesome Wayland</a>: curated list of Wayland code and resources</li>
				<li><a href="https://arewewaylandyet.com/">Are We Wayland yet?</a></li>
				<li><a href="https://github.com/ammen99/wf-recorder/wiki#using-wf-recorder-as-camera">Using wf-recorder as camera</a></li>
				<li><a href="https://ajaxnwnk.blogspot.com/2020/10/on-abandoning-x-server.html">on abandoning the X server</a></li>
			</ul>
			<h3>Own Gentoo Pull Requests</h3>
			<ul>
				<li><a href="https://github.com/gentoo/gentoo/pull/20179">dev-util/apitrace: Put libX11/mesa[X] behind USE=X</a></li>
				<li><a href="https://github.com/gentoo/gentoo/pull/20181">media-gfx/inkscape: Remove libX11</a></li>
				<li><a href="https://github.com/gentoo/gentoo/pull/20182">media-libs/waffle: Put libX11 behind existing USE=X</a></li>
			</ul>
		</main>
<!--#include file="/templates/en/footer.shtml" -->
	</body>
</html>
