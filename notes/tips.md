## Cannot remove: No space left on device
```shell
$ truncate --size 0 file_to_remove
```

## Remove empty directories
```shell
find . -type d -empty -exec rmdir "{}" \;
```
