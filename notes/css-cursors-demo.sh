#!/bin/sh

CURSORS='
auto
default
none
context-menu
help
pointer
progress
wait
cell
crosshair
text
vertical-text
alias
copy
move
no-drop
not-allowed
grab
grabbing
e-resize
n-resize
ne-resize
nw-resize
s-resize
se-resize
sw-resize
w-resize
ew-resize
ns-resize
nesw-resize
nwse-resize
col-resize
row-resize
all-scroll
zoom-in
zoom-out'

WORKDIR="$(dirname "$0")"

for cur in ${CURSORS}; do
printf '#cursors-%s { cursor: %s; }\n' "$cur" "$cur"
done >| "${WORKDIR}/css-cursors-demo.css"

(
printf \
'<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:xi="http://www.w3.org/2001/XInclude" xml:lang="en" lang="en">
	<head>
<!--#include file="/templates/head.shtml" -->
		<title>CSS cursors demo</title>
		<link rel="stylesheet" href="css-cursors-demo.css" />
	</head>
	<body>
<!--#include file="/templates/en/nav.shtml" -->
		<main>
		<h1>CSS cursors demo</h1>
		<p>You can hover on each word to get it&quot;s corresponding cursor property</p>
		<ul>
'

	for cur in ${CURSORS}; do
		printf \
'			<li id="cursors-%s">%s</li>
' "$cur" "$cur"
	done

printf \
'		</ul>
		</main>
<!--#include file="/templates/en/footer.shtml" -->
	</body>
</html>\n'
) >| "${WORKDIR}/css-cursors-demo.xhtml"
