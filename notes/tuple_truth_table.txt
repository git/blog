 0   0   0   0  ⤫ false
 0   0   0   1  ⤭ AND
 0   0   1   0  ⤫ https://en.wikipedia.org/wiki/Material_nonimplication
 0   0   1   1  ⤫ p
 0   1   0   0  ⤫ https://en.wikipedia.org/wiki/Converse_nonimplication
 0   1   0   1  ⤫ q
 0   1   1   0  ⤭ XOR, !=
 0   1   1   1  ⤭ OR
 1   0   0   0  ⤭ NOR
 1   0   0   1  ⤭ XNOR, ==
 1   0   1   0  ⤫ !q
 1   0   1   1  ⤫ https://en.wikipedia.org/wiki/Converse_implication
 1   1   0   0  ⤫ !p
 1   1   0   1  ⤫ https://en.wikipedia.org/wiki/Material_conditional
 1   1   1   0  ⤭ NAND
 1   1   1   1  ⤭ true
0,0 0,1 1,0 1,1 (p,q)

⤫ : non-commutative
⤭ : commutative

Note: Based on https://en.wikipedia.org/wiki/Truth_table#Binary_operations BUT with reordering the rows of the first table for our sort order
