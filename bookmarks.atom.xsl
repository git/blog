<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/2005/Atom" version="1.0" xml:space="preserve">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/xbel"><xsl:text disable-output-escaping="yes"><![CDATA[<?xml-stylesheet type="text/xsl" href="atom.xsl"?>]]></xsl:text>
<feed xmlns="http://www.w3.org/2005/Atom">
	<title>Bookmarks feed — Cyber-Home of Lanodan</title>
	<id>https://hacktivis.me/bookmarks.atom</id>
	<link href="https://hacktivis.me/bookmarks.atom" rel="self" />
	<link href="https://hacktivis.me/bookmarks" rel="alternate" />
	<author>
		<name>Haelwenn (lanodan) Monnier</name>
	</author>

	<!-- Technically XBEL doesn't have modified for the <xbel> element but whatever-->
	<updated><xsl:value-of select="/xbel/@modified"/></updated>

<xsl:apply-templates select="/xbel//bookmark">
	<xsl:sort select="@added" lang="en" order="descending"/>
</xsl:apply-templates>
</feed></xsl:template>

<xsl:template match="bookmark">	<entry>
		<title><xsl:value-of select="title"/></title>
		<link rel="alternate" type="text/html" href="{@href}"/>
		<id><xsl:value-of select="@href"/></id>
		<xsl:if test="@added"><published><xsl:value-of select="@added"/></published></xsl:if>
		<xsl:if test="@modified"><updated><xsl:value-of select="@modified"/></updated></xsl:if>
		<xsl:if test="desc"><content type="text"><xsl:value-of select="desc"/></content></xsl:if>
	</entry>
</xsl:template>
</xsl:stylesheet>
