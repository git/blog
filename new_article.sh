#!/bin/sh
case $# in
1)
	title="${1}"
	file="${1}"
;;
2)
	title="${2}"
	file="${1}"
;;
*)
	echo 'Usage: new_article.sh [filename] <title>'
	exit 1
;;
esac

file_url="articles/$(echo \"$file\" | jq -r '@uri')"
timestamp="$(date -u +%FT%TZ)"
ht="	"
WORKDIR="$(dirname $0)"

if grep -q -F "/articles/${file}" "${WORKDIR}/feed.atom.in"
then
	echo 'new_article: error: article already in feed.atom.in'
	exit 1
else
	ed "${WORKDIR}/feed.atom.in" <<-EOF
/new.sh: new articles here/
a
<xi:include href="${file_url}.xml"/>
.
w
q
EOF
fi

if grep -q -F "/articles/${file}" "${WORKDIR}/home.shtml"
then
	echo 'new_article: error: article already in home.shtml'
	exit 1
else
	ed "${WORKDIR}/home.shtml" <<-EOF
/new.sh: new articles here/
a
			<li>$(date -u +%F): <a href="/${file_url}">${title}</a></li>
.
w
q
EOF
fi

cat >"${WORKDIR}/articles/${file}.xml" <<-EOF
<entry>
<title>${title}</title>
<link rel="alternate" type="text/html" href="https://hacktivis.me/${file_url}"/>
<id>https://hacktivis.me/${file_url}</id>
<published>${timestamp}</published>
<updated>${timestamp}</updated>
<!--
<link rel="external replies" type="application/activity+json" href="https://queer.hacktivis.me/objects/50be9d37-dee6-4c69-818e-013fa3b010d0" />
<link rel="external replies" type="text/html" href="https://queer.hacktivis.me/objects/50be9d37-dee6-4c69-818e-013fa3b010d0" />
-->
<content type="xhtml">
<div xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" class="h-entry">

<!-- Article content goes here -->

</div>
</content>
</entry>
EOF

