#!/bin/sh
set -- $(sed -E -n 's|^\t--(ansi[^:]*): (#[a-zA-Z0-9]*);$|-e s;@\1@;\2;|p' css/colorscheme.css)
exec sed "$@"
