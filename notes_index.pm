#!/usr/bin/env perl
# Copyright 2023 Haelwenn (lanodan) Monnier <contact+blog@hacktivis.me>
# SPDX-License-Identifier: MIT

# Gentoo: dev-perl/URI
# Alpine: perl-uri
# https://repology.org/project/perl:uri/versions
use URI::Escape;

print '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
<!--#include file="/templates/head.shtml" -->
		<title>Index of /notes/</title>
		<link rel="stylesheet" href="/css/sorttable.css?serial=2020091801"/>
	</head>
	<body>
<!--#include file="/templates/en/nav.shtml" -->
		<main>
		<table class="sortable">
			<thead>
				<tr><th class="sorttable_sorted">Name</th><th>Size</th><th>Last Modified</th></tr>
			</thead>
			<tbody>
';

chdir('./notes/');

my $lstree =
`git ls-tree -r -z --format="%(objecttype)%x01%(objectsize:padded)%x01%(path)" HEAD`;

my @list = split( /\x00/, $lstree );

foreach (@list) {
	my ( $objtype, $size, $raw ) = split( /\x01/, $_, 3 );
	$objtype == "blob" or die "${raw}: objtype is ${objtype}, not blob";

	if ( $raw =~ /^index\.(s|x)html$/ ) {
		next;
	}
	if ( $raw =~ /\.(br|gz)$/ ) {
		next;
	}

	# try_files $uri $uri.shtml $uri.html $uri.xhtml $uri/ =404;
	my $fn = $raw;
	$fn =~ s@\.(s|x)?html$@@;

	my $escaped = uri_escape_utf8($fn);
	$escaped =~ s@%2F@/@;

	my $now = `git log -1 --format="format:%ai" -- "$raw"`;

	$size =~ s/^ *//;

	# thousand separation
	my $size_pp = $size;
	while ( $size_pp =~ s/(\d+)(\d{3})/$1 $2/ ) { }

	printf
"				<tr><td><a href=\"%s\">%s</a></td><td data-value=\"%s\" data-type=\"int\">%s B</td><td>%s</td></tr>\n",
	  $escaped, $fn, $size, $size_pp, $now;
}

print '			</tbody>
			</table>
		</main>
<!--#include file="/templates/en/footer.shtml" -->
		<script src="/javascript/sorttable.js?serial=2023030902"></script>
	</body>
</html>
';
