<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xml:space="preserve">
	<xsl:template match="xhtml:navbar">
	<div><xsl:call-template name="nav"/></div>
	</xsl:template>

	<xsl:param name="feedURL"/>
	<xsl:template name="nav">
		<header>I, too, "value your privacy" but unlike most I think it is priceless and fundamental. <a href="/privacy%20policy">Privacy Policy</a></header>
		<nav><details open="">
			<summary>Links</summary>
			<ul>
				<li><a href="/home">Home</a></li>
				<li><a href="/about">About</a></li>
				<li><a href="/projects/">Software Projects</a></li>
				<li><a href="/animelist">Anime List</a></li>
				<li><a href="/mangalist">Manga List</a></li>
				<li><a href="/bookmarks">Bookmarks</a></li>
				<li><a href="/coding%20style">coding style</a></li>
				<li><a href="/decreases%20of%20usability">Decreases of usability</a></li>
				<li><a href="/software%20basic%20needs">Software basic requirements</a></li>
				<li><a href="/recaptcha">Google ReCaptcha</a></li>
				<li><a href="/git/">/git/</a></li>
				<li><a href="/kopimi/">/kopimi/</a>: libre data</li>
				<li><a href="/librism">Désintox’ / FOSS activism</a></li>
				<li><a href="/notes/">/notes/</a></li>
				<li><a href="/standards">standards</a>: opinions on them</li>
			</ul>
			<ul><xsl:if test="$feedURL">
				<li><a rel="alternate" type="application/atom+xml" href="{$feedURL}">Atom feed</a></li></xsl:if>
				<li><a href="gemini://hacktivis.me/">gemini-space</a></li>
				<li><a href="https://lanodan.eu/home">Resume</a></li>
			</ul>
			<ul>
				<li><a href="https://liberapay.com/lanodan_">Donations</a></li>
			</ul>
		</details></nav>
	</xsl:template>
</xsl:stylesheet>
