<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xml:space="preserve">
	<xsl:param name="feedURL"/>
	<xsl:template name="nav">
		<nav><details open="">
			<summary>Liens</summary>
			<ul>
				<li><a href="/accueil">Accueil</a></li>
				<li><a href="/%C3%A0%20propos">À propos</a></li>
				<li><a href="/projects/">Projets Logiciels</a></li>
				<li><a href="/animelist">Anime List(en)</a></li>
				<li><a href="/bitoduc">Bitudoc</a>: termes info’ en français</li>
				<li><a href="/git/">/git/</a></li>
				<li><a href="/kopimi/">/kopimi/</a>: données libres)</li>
				<li><a href="/librisme">Désintox’ / Engagement libriste</a></li>
				<li><a href="/notes/">/notes/</a></li>
				<li><a href="https://xn--lwifi-sra.fr/">læwifi.fr</a></li>
			</ul>
			<ul><xsl:if test="$feedURL">
				<li><a rel="alternate" type="application/atom+xml" href="{$feedURL}">Flux Atom</a></li></xsl:if>
				<li><a href="gemini://hacktivis.me/">espace gemini</a></li>
				<li><a href="https://lanodan.eu/accueil">CV</a></li>
			</ul>
			<ul>
				<li><a href="https://liberapay.com/lanodan_">Donations</a></li>
			</ul>
		</details></nav>
	</xsl:template>
</xsl:stylesheet>
